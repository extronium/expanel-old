<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<title>ExPanel - Extronium Hosting</title>

		<link rel="stylesheet" href="assets/themes/home/css/main.css">
	</head>
	<body>
		<div class="header">
			<div class="bars">
				<a href="#">
					<div class="bar"></div>
					<div class="bar"></div>
					<div class="bar"></div>
				</a>
			</div>
			<div class="title">
				<p>Extronium Control Panel</p>
			</div>
			<div class="right">
				<a href="#" class="btn btn-small btn-normal">Log out</a>
			</div>
		</div>	
		<div class="container">
			<b>Current Plan:</b>
			<a href="#">Business Plan</a><span> $16/month</span>
			<br>
			<br>
			<br>
			<p>Next Bill:</p>
			<b>24th May 2015</b>
			<br>
			<br>
			<br>
			<p>Plan Details</p>
			<ul>
				<li>4096MB Dedicated RAM</li>
				<li>Free Dedicated IP</li>
				<li>Unlimited Player Slots</li>
				<li>Custom Jar Support</li>
				<li>BukGet Plugin Intergration</li>
				<li>Full FTP Access</li>
				<li>Multicraft Control Panel</li>
				<li>Premium Support</li>
			</ul>
			<br>
			<br>
			<br>
			<a href="#" class="btn btn-small btn-danger">Cancel Plan</a>
			<br>
			<p>Cancelling your plan will terminate</p>
			<p>any and all servers currently associated with your account</p>
		</div>
		<div class="right">
			<div class="side-panel">
				<p>Account Information</p>
			</div>
		</div>
	</body>
</html>