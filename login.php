<?php
require_once "Library/init.php";
?>
<!doctype html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">

		<title>ExPanel - Extronium Hosting</title>

		<link rel="stylesheet" href="assets/themes/login2/css/main.css"> 
		<link rel="stylesheet" href="assets/alert/sweetalert.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	</head>
	<body>
		<div class="container">
			<div class="left">
				<h2 style="font-size: 1.5em">Welcome to the Extronium Control Panel</h2>
				<p>Sign in to get started.</p>
				<p>Don't have an account? <a href="register">Sign up here</a></p>
				<div class="back">
					<a href="https://www.extroniumhosting.com"><i class="fa fa-home"></i>Go back to home</a>
				</div>
			</div>
			<div class="right">
				<div class="form">
					<h2><b>ExPanel</b> Sign in</h2>
					<form method="POST" id="login-form">
						<input type="text" id="username" name="username" placeholder="Username">
						<input type="password" id="password" name="password" placeholder="Password">
						<div id="remember-me">
							<input type="checkbox" id="remember"><label for="remember-me">Remember me</label>
						</div>
						<div id="ip-changes">
							<input type="checkbox" id="cip"><label for="ip-changes">Allow IP Changes</label>
						</div>
						<div class="button">
							<input type="submit" value="Sign in">
						</div>
					</form>
				</div>
			</div>
		</div>
		<script src="assets/scripts/jquery.js"></script>
		<script src="assets/themes/login/scripts/main.js"></script>
		<script src="assets/alert/sweetalert.min.js"></script>
		<?php if(isset($_SESSION['isCIP'])) { unset($_SESSION['isCIP']); echo "<script>sweetAlert(\"Error\", \"You have been logged out because your IP has changed.\", \"error\");</script>";} ?>
		<?php if(isset($_SESSION['isExpire'])) { unset($_SESSION['isExpire']); echo "<script>sweetAlert(\"Error\", \"You have been logged out because your session expired.\", \"error\");</script>";} ?>
	</body>
</html>