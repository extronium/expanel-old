<?php
class User{
	public static function isValid($user, $pass) {
		$db = Database::getDatabase();
		$res = $db->query("SELECT password FROM user WHERE name=?", array($user));
		if($res->error) {
			return false;
		}
		if($res->count == 0) {
			return false;
		} else {
			if($res->results[0]->password === crypt($pass, $res->results[0]->password)) {
				return true;
			} else {
				return false;
			}
		}
	}

	public static function getUser($user) {
		$db = Database::getDatabase();
		$res = $db->query("SELECT id,global_role FROM user WHERE name=?", array($user));
		if($res->error) {
			return null;
		}
		if($res->count == 0) {
			return null;
		}
		if($user == "admin") {
			$res->results[0]->global_role = "superuser";
		}
		return array("name"=>$user, "id"=>$res->results[0]->id, "role"=>$res->results[0]->global_role);
	}

	public static function checkUser() {
		if(isset($_SESSION['user'])) {
			if($_SESSION['user']['expire'] < time()) {
				$_SESSION['isExpire'] = true;
				unset($_SESSION['user']);
				header("Location: login");
				exit();
			}
			if(!$_SESSION['user']['cip']) {
				if($_SESSION['user']['ip'] != $_SERVER['REMOTE_ADDR']) {
					$_SESSION['isCIP'] = true;
					unset($_SESSION['user']);
					header("Location: login");
					exit();
				}
			}
		} else {
			header("Location: login");
			exit();
		}
	}

	public static function isEmailValid($email) {
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return "false";
		}
		$split = explode("@", $email);
		if(count($split) != 2) {
			return "false";
		}
		$db = Database::getDatabase();
		$res = $db->query("SELECT domain FROM banned_emails WHERE domain=?", array($split[1]));
		if($res->error) {
			return "false";
		}
		if($res->count != 0) {
			return "false";
		}
		$val = new SMTP_Validate_Email($email, 'support@extroniumhosting.com');
		$res = $val->validate();
		if($res[$email]) {
			return "true";
		}
		return "false";
	}


}