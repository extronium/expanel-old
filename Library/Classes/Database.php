<?php
class Database {
	private static $_instance = null;
	public $pdo, 
			$query, 
			$error=false, 
			$error_msg, 
			$results, 
			$count=0;


	private function __construct() {
		try {
			$this->pdo = new PDO('mysql:host=127.0.0.1;dbname=multicraft_panel', 'multicraft_panel', 'WBBqM2rDUVc3fGUu');
		} catch(PDOException $e) {
			die($e->getMessage());
		}
	}

	public static function getDatabase() {
		if(!isset(self::$_instance)) {
			self::$_instance = new Database();
		}
		return self::$_instance;
	}

	public function query($sql, $params = array()) {
		$this->error = false;
		if($this->query = $this->pdo->prepare($sql)) {
			$x = 1;
			if(count($params)) {
				foreach($params as $param) {
					$this->query->bindValue($x, $param);
					$x++;
				}
			}

			if($this->query->execute()) {
				$this->results = $this->query->fetchAll(PDO::FETCH_OBJ);
				$this->count = $this->query->rowCount();
			} else {
				$this->error = true;
				$this->error_msg = $this->query->errorInfo();
			}
		}

		return $this;
	}

}