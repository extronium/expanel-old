<?php
require_once "Library/init.php";
if(!isset($_POST['action'])) {
	echo "false";
	exit();
}
//var_dump($_POST);
switch($_POST['action']) {
	case "login":
		if(!isset($_POST['username'], $_POST['password'])) {
			echo "false";
			exit();
		}
		if(User::isValid($_POST['username'], $_POST['password'])) {
			echo "true";
			$_SESSION['user'] = User::getUser($_POST['username']);
			if(isset($_POST['remember'])) {
				$_SESSION['user']['time'] = 31536000;
			} else {
				$_SESSION['user']['time'] = 43200;
			}
			$_SESSION['user']['expire'] = floor(time() + $_SESSION['user']['time']);
			if(isset($_POST['cip'])) {
				$_SESSION['user']['cip'] = true;
			} else {
				$_SESSION['user']['cip'] = false;
			}
			$_SESSION['user']['ip'] = $_SERVER['REMOTE_ADDR'];
		} else {
			echo "false";
		}
		break;
	case "logout":
		if(isset($_SESSION['user'])) {
			unset($_SESSION['user']);
			echo "true";
			exit();
		} else {
			echo "false";
			exit();
		}
		break;
	case "verifyEmail":
		if(!isset($_POST['email'])) {
			echo "false";
			exit();
		}
		echo User::isEmailValid($_POST['email']);
		break;
	default:
		echo "false";
}