<?php
require_once "Library/init.php";
?>
<!doctype html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">

		<title>ExPanel - Extronium Hosting</title>

		<link rel="stylesheet" href="assets/themes/login2/css/main.css"> 
		<link rel="stylesheet" href="assets/alert/sweetalert.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

		<style>
			.form {
				height: 500px;
			}
			.rc-anchor {
				width: 500px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="left">
				<h2 style="font-size: 1.5em">Welcome to the Extronium Control Panel</h2>
				<p>Sign up now!</p>
				<p>Have an account? <a href="login">Sign in here</a></p>
				<div class="back">
					<a href="https://www.extroniumhosting.com"><i class="fa fa-home"></i>Go back to home</a>
				</div>
			</div>
			<div class="right">
				<div class="form">
					<h2><b>ExPanel</b> Sign up</h2>
					<form method="POST" id="register-form">
						<input type="text" id="email" name="email" placeholder="Email">
						<input type="password" id="password" name="password" placeholder="Password">
						<br>
						<input type="password" id="c-password" name="c-password" placeholder="Confirm Password">
						<center><div class="g-recaptcha" data-sitekey="6LcB4gUTAAAAANlzT3j8i3wVOjjI-1seuLjZtnra"></div></center>
						<div class="button">
							<input type="submit" value="Sign up">
						</div>
					</form>
				</div>
			</div>
		</div>
		<script src="assets/scripts/jquery.js"></script>
		<script src="assets/themes/login/scripts/main.js"></script>
		<script src="assets/alert/sweetalert.min.js"></script>
		<script src='https://www.google.com/recaptcha/api.js'></script>
	</body>
</html>