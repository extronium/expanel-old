<?php
require_once "Library/init.php";
User::checkUser();
echo "Your username is: " . $_SESSION['user']['name'];
echo "<br>Your ID is: " . $_SESSION['user']['id'];
echo "<br>Your global role is: " . $_SESSION['user']['role'];
echo "<pre>";
var_dump($_SESSION);
echo "</pre>";
?>
<form method="POST" id="login-form">
				<input type="submit" value="Log Out">
			</form>
<script src="assets/scripts/jquery.js"></script>
<script>
$(function() {
	$('#login-form').submit(function(e) {
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: 'api.php',
			data: 'action=logout',
			success: function(data) {
				window.location.replace('index.php');
			}
		});
	});	
});
</script>