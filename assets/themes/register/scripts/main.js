$(function() {
	$('#register-form').submit(function(e) {
		e.preventDefault();
		var username = $('#username').val();
		var password = $('#password').val();
		var cpassword = $('#c-password').val();
		var email = $('#email');

		var verifyEmail = 'action=verifyEmail&email=' + email;

		if(password !=== cpassword) {
			sweetAlert('Error', "Your passwords don't seem to match!", 'error');
		}
		if(!validateEmail(email)) {
			sweetAlert('Error', "Please enter a valid email address", 'error');
		}
		$.ajax({
			type: 'POST',
			url: 'api.php',
			data: verifyEmail,
			success: function(data) {
				if(data === 'true') {
					$.ajax({
						type: 'POST', 
						url: 'api.php',
						data: postData,
						success: function(data) {
							if(data === 'true') {
								window.location.replace('login.php');
							} else {
								if(data === 'false') {
									sweetAlert('Error', "Please review your form credentials", 'error');
								}
							}
						}
					});
				} else {
					if(data === 'false') {
						sweetAlert('Error', "Please enter a valid email address", 'error');
					}
				}
			}
		});
	});

	function validateEmail(email) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(email);		
	}
});