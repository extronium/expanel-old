$(function() {
	$('#login-form').submit(function(e) {
		e.preventDefault();
		var username = $('#username').val();
		var password = $('#password').val();
		var remember = $('#remember').is(':checked');
		var cip = $('#cip').is(':checked');
		var postData = 'action=login&username=' + username + '&password=' + password;
		
		if(remember) {
			postData = postData.concat('&remember=true');
		}
		if(cip) {
			postData = postData.concat('&cip=true');
		}
		$.ajax({
			type: 'POST',
			url: 'api.php',
			data: postData,
			success: function(data) {
				if(data === 'true') {
					window.location.replace('/');
				} else {
					if(data === 'false') {
						sweetAlert("Error", "The Username and Password you entered is invalid!", "error");
					}
				}
			}
		});
	});	
});